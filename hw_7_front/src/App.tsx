//import React from 'react';
import { useEffect, useState } from 'react';
import './App.css';

interface WeatherForecast {
        date: Date,
        temperatureC: number,
        temperatureF: number,
        summary: string
}
function App() {
    const BACK_URL = 'https://localhost:7251/weatherForecast';
    const [forecasts, setForecasts] = useState<Array<WeatherForecast>>();
    const [err, setErr] = useState<any>();

    useEffect(() => {
        populateWeatherData();
    }, []);

    console.log('App(): forecasr=', forecasts);
    console.log('App(): err=', err);
    //console.log('App(): err|forecast', err | forecasts);
    const contents = forecasts === undefined
    //const contents = err === null
        ? <p>
            <em>{
                err?.name + ': ' + err?.message ?? 'Loading...' }
            </em></p>

        : <table className="table table-striped" aria-labelledby="tabelLabel">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Temp. (C)</th>
                    <th>Temp. (F)</th>
                    <th>Summary</th>
                </tr>
            </thead>
            <tbody>
                {forecasts.map(forecast =>
                    <tr key={forecast.date + ''}>
                        <td>{forecast.date.toString()}</td>
                        <td>{forecast.temperatureC}</td>
                        <td>{forecast.temperatureF}</td>
                        <td>{forecast.summary}</td>
                    </tr>
                )}
            </tbody>
        </table>;

  return <>
      <h1>HW_7_front</h1>
          <div>
            <h1 id="tabelLabel">Weather forecast</h1>
            <p>This component demonstrates fetching data from the server using CORS policy</p>
            {contents}
           
      </div>
  </>
  
    async function populateWeatherData() {
        console.log('call populateWeatherData');
        setErr(null);

        try {
            const response = await fetch(BACK_URL);

            if (!response.ok)
                throw new Error(`response.status=${response.status}, url=${response.url}`)

            console.log('status=', response.status + ": " + response.statusText);
            console.log('response=', response);
            const data = await response.json();
            setForecasts(data);

        }
        catch (err: any) {
            //console.log('type of err is ', typeof(err));
            //console.log('err=', err);
            //console.log('name=', err.name);
            //console.log('message=', err.message);
            //console.log('err instanceof ResponseError', err instanceof(ResponseError))
            
            //err.response.status 

            setErr(err);
            console.log('catch()', err);
        }

    }
}

export default App;